FROM python:3.8

RUN apt-get -yq update && \
    apt-get -yq install python3-pip && \
    python -m pip install --upgrade pip && \
    rm -rf /var/lib/apt/lists/*

WORKDIR /app

COPY . /app

RUN pip install -r requirements.txt

ENTRYPOINT ["python3", "app.py"]

EXPOSE 5000
